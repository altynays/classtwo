package com.company;

public class Stone {
    private String name;
    private double transparency;
    private double weight;
    private double price;

    public Stone() {
    }

    public Stone(String name, double transparency, double weight, double price) {
        this.name = name;
        this.transparency = transparency;
        this.weight = weight;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTransparency() {
        return transparency;
    }

    public void setTransparency(double transparency) {
        this.transparency = transparency;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Stone{" + "name='" + name + '\'' + ", transparency=" + transparency + ", weight=" + weight +
                ", price=" + price + '}';
    }

    @Override
    public boolean equals(Object o) {
        Stone stone = (Stone) o;

        if (Double.compare(stone.price, price) != 0) return false;
        return name != null ? name.equals(stone.name) : stone.name == null;
    }

}
