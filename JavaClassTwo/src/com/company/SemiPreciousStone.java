package com.company;

public class SemiPreciousStone extends Stone {

    public SemiPreciousStone() {
        super();
    }

    public SemiPreciousStone(String name, double transparency, double weight, double cost) {
        super(name, transparency, weight, cost);
    }

    @Override
    public String toString() {
        return "SemiPrecious" + super.toString();
    }
}

