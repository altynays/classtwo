package com.company;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Jewellery {
    List<Stone> stones;

    public Jewellery() {
    }

    public Jewellery(List<Stone> stones) {
        this.stones = stones;
    }

    public List<Stone> getStones() {
        return stones;
    }

    public void setStones(List<Stone> stones) {
        this.stones = stones;
    }

    public double calculateWeight() {
        return stones.parallelStream().map(Stone::getWeight).reduce(0.0, (a, b) -> a + b);
    }

    public double calculatePrice() {
        return stones.parallelStream().map(Stone::getPrice).reduce(0.0, (a, b) -> a + b);
    }

    public List<Stone> sortStonesByPrice() {
        return sortStonesByPrice(true);
    }

    public List<Stone> sortStonesByPrice(boolean ascending) {
        Comparator<Stone> comparator = new StoneComparator();
        return ascending ?
                stones.parallelStream().sorted(comparator).collect(Collectors.toList()) :
                stones.parallelStream().sorted(comparator.reversed()).collect(Collectors.toList());
    }
    public class StoneComparator implements Comparator<Stone> {

        @Override
        public int compare(Stone o1, Stone o2) {
            return Double.compare(o1.getPrice(), o2.getPrice());
        }
    }

    public List<Stone> filterStonesByTransparency(double from, double to) {
        return stones.parallelStream()
                .filter(stone -> stone.getTransparency() >= from && stone.getTransparency() <= to)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Jewellery{" +
                "stones=" + stones +
                '}';
    }
}

