package com.company;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Stone> stones = Arrays.asList(
                new PreciousStone("Diamond", 0.7, 180, 450),
                new PreciousStone("Ruby", 0.8, 345, 370),
                new PreciousStone("Sapphire", 0.9, 250, 450),
                new PreciousStone("Emerald", 0.65, 125, 380),
                new SemiPreciousStone("Azurite", 0.2, 200, 180),
                new SemiPreciousStone("Calcite", 0.3, 270, 195),
                new SemiPreciousStone("Beryl", 0.4, 225, 178)
        );
        Jewellery jewellery = new Jewellery(stones);

        System.out.println("Jewellery contains:");
        System.out.println(jewellery);

        System.out.println(String.format("Jewellery Cost: %f", jewellery.calculatePrice()));
        System.out.println(String.format("Jewellery Weight: %f", jewellery.calculateWeight()));

        System.out.println("Stones sorted by price:");
        System.out.println(jewellery.sortStonesByPrice());

        System.out.println("Stones with transparency from 0.6 to 0.7:");
        System.out.println(jewellery.filterStonesByTransparency(0.7, 0.8));
    }
}

