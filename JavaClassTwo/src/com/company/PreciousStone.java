package com.company;

public class PreciousStone extends Stone {

    public PreciousStone() {
        super();
    }

    public PreciousStone(String name, double transparency, double weight, double cost) {
        super(name, transparency, weight, cost);
    }

    @Override
    public String toString() {
        return "Precious" + super.toString();
    }
}
